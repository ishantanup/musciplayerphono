package com.apkmirrorapps.mp3musicplayer.helper.menu;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.apkmirrorapps.mp3musicplayer.App;
import com.apkmirrorapps.mp3musicplayer.dialogs.AddToPlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.dialogs.DeletePlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.dialogs.RenamePlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.loader.PlaylistSongLoader;
import com.apkmirrorapps.mp3musicplayer.App;
import com.apkmirrorapps.mp3musicplayer.R;
import com.apkmirrorapps.mp3musicplayer.dialogs.AddToPlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.dialogs.DeletePlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.dialogs.RenamePlaylistDialog;
import com.apkmirrorapps.mp3musicplayer.helper.MusicPlayerRemote;
import com.apkmirrorapps.mp3musicplayer.loader.PlaylistSongLoader;
import com.apkmirrorapps.mp3musicplayer.misc.WeakContextAsyncTask;
import com.apkmirrorapps.mp3musicplayer.model.AbsCustomPlaylist;
import com.apkmirrorapps.mp3musicplayer.model.Playlist;
import com.apkmirrorapps.mp3musicplayer.model.Song;
import com.apkmirrorapps.mp3musicplayer.util.PlaylistsUtil;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Karim Abou Zeid (kabouzeid)
 */
public class PlaylistMenuHelper {
    public static boolean handleMenuClick(@NonNull AppCompatActivity activity, @NonNull final Playlist playlist, @NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_play:
                MusicPlayerRemote.openQueue(new ArrayList<>(getPlaylistSongs(activity, playlist)), 0, true);
                return true;
            case R.id.action_play_next:
                MusicPlayerRemote.playNext(new ArrayList<>(getPlaylistSongs(activity, playlist)));
                return true;
            case R.id.action_add_to_current_playing:
                MusicPlayerRemote.enqueue(new ArrayList<>(getPlaylistSongs(activity, playlist)));
                return true;
            case R.id.action_add_to_playlist:
                AddToPlaylistDialog.create(new ArrayList<>(getPlaylistSongs(activity, playlist))).show(activity.getSupportFragmentManager(), "ADD_PLAYLIST");
                return true;
            case R.id.action_rename_playlist:
                RenamePlaylistDialog.create(playlist.id).show(activity.getSupportFragmentManager(), "RENAME_PLAYLIST");
                return true;
            case R.id.action_delete_playlist:
                DeletePlaylistDialog.create(playlist).show(activity.getSupportFragmentManager(), "DELETE_PLAYLIST");
                return true;
            case R.id.action_save_playlist:
                new SavePlaylistAsyncTask(activity).execute(playlist);
                return true;
        }
        return false;
    }

    @NonNull
    private static ArrayList<? extends Song> getPlaylistSongs(@NonNull Activity activity, Playlist playlist) {
        return playlist instanceof AbsCustomPlaylist ?
                ((AbsCustomPlaylist) playlist).getSongs(activity) :
                PlaylistSongLoader.getPlaylistSongList(activity, playlist.id);
    }


    private static class SavePlaylistAsyncTask extends WeakContextAsyncTask<Playlist, String, String> {
        public SavePlaylistAsyncTask(Context context) {
            super(context);
        }

        @Override
        protected String doInBackground(Playlist... params) {
            try {
                return String.format(App.getInstance().getApplicationContext().getString(R.string.saved_playlist_to), PlaylistsUtil.savePlaylist(App.getInstance().getApplicationContext(), params[0]));
            } catch (IOException e) {
                e.printStackTrace();
                return String.format(App.getInstance().getApplicationContext().getString(R.string.failed_to_save_playlist), e);
            }
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            Context context = getContext();
            if (context != null) {
                Toast.makeText(context, string, Toast.LENGTH_LONG).show();
            }
        }
    }
}
