package com.apkmirrorapps.mp3musicplayer;

import android.app.Application;
import android.os.Build;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.ads.MobileAds;
import com.kabouzeid.appthemehelper.ThemeStore;
import com.apkmirrorapps.mp3musicplayer.BuildConfig;
import com.apkmirrorapps.mp3musicplayer.R;
import com.apkmirrorapps.mp3musicplayer.appshortcuts.DynamicShortcutManager;


/**
 * @author Karim Abou Zeid (kabouzeid)
 */
public class App extends Application {
    public static final String TAG = App.class.getSimpleName();

    private static App app;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        MobileAds.initialize(this, getString(R.string.app_id));

        // default theme
        if (!ThemeStore.isConfigured(this, 1)) {
            ThemeStore.editTheme(this)
                    .primaryColorRes(R.color.color_dark_primary)
                    .accentColorRes(R.color.color_dark_accent)
                    .commit();
        }

        // Set up dynamic shortcuts
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            new DynamicShortcutManager(this).initDynamicShortcuts();
        }

    }


    public static App getInstance() {
        return app;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
