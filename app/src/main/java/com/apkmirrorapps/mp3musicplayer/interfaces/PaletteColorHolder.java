package com.apkmirrorapps.mp3musicplayer.interfaces;

import android.support.annotation.ColorInt;

/**
 * @author Aidan Follestad (afollestad)
 */
public interface PaletteColorHolder {

    @ColorInt
    int getPaletteColor();
}
